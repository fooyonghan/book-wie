import json, datetime, operator, re
from pprint import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

def getTagCount(data):
	tagCount = {}

	for entry in data:
		if 'tags' in entry:
			tags = entry['tags']

			for tag in tags:
				if tag in tagCount:
					tagCount[tag] += 1 
				else:
					tagCount[tag] = 1

	# sortedTagCount = sorted(tagCount.items(), key=operator.itemgetter(1))
	return tagCount

def getTimePostedCount(data):
	timePostedCount = {
		'00': 0,
		'01': 0,
		'02': 0,
		'03': 0,
		'04': 0,
		'05': 0,
		'06': 0,
		'07': 0,
		'08': 0,
		'09': 0,
		'10': 0,
		'11': 0,
		'12': 0,
		'13': 0,
		'14': 0,
		'15': 0,
		'16': 0,
		'17': 0,
		'18': 0,
		'19': 0,
		'20': 0,
		'21': 0,
		'22': 0,
		'23': 0
	}

	for entry in data:
		if 'taken_at_timestamp' in entry:

			readableTime = datetime.datetime.fromtimestamp(entry['taken_at_timestamp']).isoformat()
			date, time = readableTime.split("T")
			hour, minute, second = time.split(":")

			timePostedCount[hour] += 1

	plotTimePostedCount(timePostedCount)
	return timePostedCount

def plotTimePostedCount(timePostedCount):
	timeDataFrame = pd.DataFrame(timePostedCount.items(), columns=['O\'Clock', 'Number of Posts'])
	sns.barplot(x='O\'Clock', y='Number of Posts', data=timeDataFrame, color="lightgreen")
	plt.show()

def getEntryUrl(data):
	for entry in data:
		if 'display_url' in entry:
			entryUrl = entry['display_url']
			found = re.search('/e35/(.+?).jpg', entryUrl)
			if found:
				imgName = found.group(1)

def findLikes(filename, data):
	for entry in data:
		entryUrl = entry['display_url']
		found = re.search('/e35/(.+?).jpg', entryUrl)
		if found:
			imgName = found.group(1)
			filename = filename.replace('.jpg', '')
			if imgName == filename:
				likes = entry['edge_media_preview_like']['count']

				return likes

def findTags(filename, data):
	for entry in data:
		entryUrl = entry['display_url']
		found = re.search('/e35/(.+?).jpg', entryUrl)
		if found:
			imgName = found.group(1)
			filename = filename.replace('.jpg', '')
			if imgName == filename:
				tags = entry['tags']

				return tags 

def getLikeCount(data):
	# Most liked post: 1515183370387445477
	likeCount = {}

	for entry in data:
		if 'edge_media_preview_like' in entry:
			entryId = entry['id']
			numberOfLikes = entry['edge_media_preview_like']['count']

			likeCount[entryId] = numberOfLikes

	# sortedLikeCount = sorted(likeCount.items(), key=operator.itemgetter(1))
	return likeCount

# Not enough data for trustworthy results
def getLocation(data):
	locationList = []
	for entry in data:
		if 'location' in entry:
			location = entry['location']

			if location is not None:
				locationList.append(location)
	return locationList

def getEntryWordCount(data):
	wordCount = {}

	for entry in data:
		if 'edge_media_to_caption' in entry:
			edges = entry['edge_media_to_caption']['edges']

			if edges:
				caption = edges[0]['node']['text']
				words = caption.split(" ")

				for word in words:
					if word in wordCount:
						wordCount[word] += 1 
					else:
						wordCount[word] = 1

	sortedWordCount = sorted(wordCount.items(), key=operator.itemgetter(1))

	noHashWordCount = {}
	for word, count in sortedWordCount:
		if not '#' in word:
			noHashWordCount[word] = count

	sortedNoHashWordCount = sorted(noHashWordCount.items(), key=operator.itemgetter(1))
	return sortedNoHashWordCount

def getUserCommentCount(data):
	users = {}

	for entry in data:
		comments = entry['comments']['data']

		if comments:
			for comment in comments:
				username = comment['owner']['username']

				if username in users:
					users[username] += 1
				else:
					users[username] = 1

	sortedUsers = sorted(users.items(), key=operator.itemgetter(1))
	return sortedUsers