# Used scrape command:
# 	instagram-scraper quienbusco --media-metadata --include-location --comments --latest --retry-forever
import json
from pprint import pprint
from tools import *
from book import *
from fpdf import FPDF

json_data=open("quienbusco-raw-everything/quienbusco.json").read()
data = json.loads(json_data)

# Print other pages
pdf = PDF(orientation = 'P', unit = 'mm', format='A5')
pdf.alias_nb_pages()
pdf.set_font('Helvetica', '', 12)
pdf.print_page(data)
pdf.print_data_chapter_page()
pdf.print_when_wie_was_woke(data)
pdf.print_hashtag_haiku(data)
pdf.print_total_love_received(data)
pdf.print_final_page()

pdf.output('pdfs/quienbusco.pdf', 'F')