import os, random
from pprint import pprint
from fpdf import FPDF
from tools import *
import operator
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS

class PDF(FPDF):
	def footer(self):
		self.set_y(-15)
		self.set_font('Helvetica', 'B', 8)
		self.set_text_color(0, 0, 0)
		self.cell(0, 10, str(self.page_no()), 0, 0, 'C')

	# Print each haiku with accompanying likes and random generated design elements
	def print_page(self, data):
		pagePosition = 0
		for filename in os.listdir("quienbusco-haiku"):
			if filename.endswith(".jpg"):
				self.add_page()

				self.set_font('Helvetica', 'B', 9)
				self.set_text_color(255, 255, 255)
				self.set_fill_color(0, 128, 0)

				numOfLikes = findLikes(filename, data)

				# Switch location of number of likes tab between pages
				# Add infinity symbol when there is no number of likes
				if pagePosition == 0:
					self.rect(x=0, y=10, w=20, h=10, style='F')
					self.image("images/origami-heart.png", x=10, y=10, w=10, h=10, type='png')
					if numOfLikes is not None:
						self.text(x=3, y=16, txt=str(numOfLikes))
					else:
						self.image("images/infinity.png", x=1, y=10, w=10, h=10, type='png')
					pagePosition = 1
				else:
					self.rect(x=129, y=10, w=20, h=10, style='F')
					self.image("images/origami-heart.png", x=129, y=10, w=10, h=10, type='png')
					if numOfLikes is not None:
						self.text(x=142, y=16, txt=str(numOfLikes))
					else:
						self.image("images/infinity.png", x=138, y=10, w=10, h=10, type='png')
					pagePosition = 0

				# Black rectangle
				self.set_draw_color(0, 0, 0)
				self.set_fill_color(0, 0, 0)
				self.rect(
					x=random.randint(10, 55),
					y=random.randint(40, 65),
					w=random.randint(50, 90),
					h=random.randint(50, 110),
					style='D'
				)

				# Green rectangle
				self.set_draw_color(0, 128, 0)
				self.set_fill_color(0, 128, 0)
				self.rect(
					x=random.randint(10, 55),
					y=random.randint(40, 65),
					w=random.randint(50, 90),
					h=random.randint(50, 110),
					style='D'
				)

				# Black outline
				if filename.endswith(".jpg"):
					self.set_draw_color(0, 0, 0)
					self.set_fill_color(0, 0, 0)
					self.rect(x=29, y=49, w=90, h=90, style='DF')

				# Haiku
				url = os.path.join("quienbusco-haiku", filename)
				self.image(url, x=30, y=50, w=88, h=88, type='jpg')

	# Print data chapter page
	def print_data_chapter_page(self):
		self.add_page()
		self.add_page()

		# Title
		self.set_font('Helvetica', 'B', 32)
		self.set_text_color(0, 128, 0)
		self.text(43, 30, 'Bits of Data')
		
		# Title design elements
		self.set_draw_color(0, 0, 0)
		self.rect(x=18, y=18, w=113, h=16, style='D')
		self.set_draw_color(0, 128, 0)
		self.rect(x=20, y=20, w=113, h=16, style='D')

		self.ln(h = 64)

		# Data text
		self.set_font('Helvetica', 'B', 12)
		self.set_text_color(108, 108, 108)

		self.cell(15)
		self.cell(100, 12, 'Data scraped at: Thursday 22 March 2018', 0, 1, 'C')

		self.cell(100, 12, '', 0, 1, 'C')

		self.cell(15)
		self.cell(100, 12, 'Number of Instagram Posts: 496', 0, 1, 'C')

		self.cell(100, 12, '', 0, 1, 'C')

		self.cell(15)
		self.cell(100, 12, 'Of which were Haikus: 106', 0, 1, 'C')

	# Print page with table that represents the times images were posted
	def print_when_wie_was_woke(self, data):
		self.add_page()

		# Title
		self.set_font('Helvetica', 'B', 24)
		self.set_text_color(0, 128, 0)
		self.text(32, 30, 'When Wie Was Woke')
		
		# Table
		self.image('images/time-posted.png', x=30, y=65, w=88, h=66, type='png')

	# Create a wordcloud out of the tags used
	# Bug: weird double rendering of tags
	def print_wordcloud(self, data):
		tagCount = getTagCount(data)
		textString = ''
		
		for tagName, timesTagsUsed in tagCount.iteritems():
			for x in range (0, timesTagsUsed):
				textString += (tagName + ' ')
		
		textUtf8 = textString.encode('utf8', 'replace')
		with open("wordcloudText.txt", "w") as wordcloudText:
			wordcloudText.write(textUtf8)

		text = open('wordcloudText.txt').read()

		wc = WordCloud().generate(text)
		wc.to_file('images/clarenceCloud.png')

	# Print page with a haiku of the top 17 used hashtags
	def print_hashtag_haiku(self, data):
		self.add_page()
		tagCount = getTagCount(data)

		# Get top 17 hashtags used
		tagListHaiku = []
		for x in range(0, 17):
			maxTagCount = max(tagCount.iteritems(), key=operator.itemgetter(1))[0]
			tagListHaiku.append(maxTagCount)

			del tagCount[maxTagCount]

		# Divide top 17 hashtags into separate lines, equal to 5-7-5
		# Fout lines are used to fit the page width
		line1 = ' '.join(tagListHaiku[:5])
		line2 = ' '.join(tagListHaiku[5:11])
		line3 = ' '.join(tagListHaiku[11:12])
		line4 = ' '.join(tagListHaiku[12:17])

		haiku = []
		haiku.append(line1)
		haiku.append(line2)
		haiku.append(line3)
		haiku.append(line4)

		# Title
		self.set_font('Helvetica', 'B', 24)
		self.set_text_color(0, 128, 0)
		self.text(43, 30, 'Hashtag Haiku')

		self.ln(h = 18)

		# Subtext
		self.set_font('Helvetica', 'B', 6)
		self.set_text_color(0, 0, 0)
		self.cell(12)
		self.cell(100, 12, '(17 most used hashtags)', 0, 1, 'C')

		self.ln(h = 35)
		
		# Haiku text
		self.set_font('Helvetica', 'B', 12)
		self.set_text_color(108, 108, 108)

		self.cell(15)
		self.cell(100, 12, line1, 0, 1, 'C')

		self.cell(100, 12, '', 0, 1, 'C')
		
		self.cell(15)
		self.cell(100, 12, line2, 0, 1, 'C')

		self.cell(15)
		self.cell(100, 12, line3, 0, 1, 'C')

		self.cell(100, 12, '', 0, 1, 'C')

		self.cell(15)
		self.cell(100, 12, line4, 0, 1, 'C')

	# Print page with the total amount of likes received
	def print_total_love_received(self, data):
		self.add_page()
		
		# Title
		self.set_font('Helvetica', 'B', 24)
		self.set_text_color(0, 128, 0)
		self.text(32, 30, 'Total Love Received')

		# Heart image
		self.image("images/origami-heart-green.png", x=30, y=60, w=88, h=88, type='png')

		# Print total love received
		self.set_font('Helvetica', 'B', 32)
		self.set_text_color(255, 255, 255)
		self.text(57, 103, str(sum(getLikeCount(data).values())))

	# Print the final page
	def print_final_page(self):
		self.add_page()
		self.add_page()

		# Black rectangle
		self.set_draw_color(0, 0, 0)
		self.set_fill_color(0, 0, 0)
		self.rect(
			x=random.randint(10, 55),
			y=random.randint(40, 65),
			w=random.randint(50, 90),
			h=random.randint(50, 110),
			style='D'
		)

		# Green rectangle
		self.set_draw_color(0, 128, 0)
		self.set_fill_color(0, 128, 0)
		self.rect(
			x=random.randint(10, 55),
			y=random.randint(40, 65),
			w=random.randint(50, 90),
			h=random.randint(50, 110),
			style='D'
		)













