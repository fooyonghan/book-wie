# Used scrape command:
# 	instagram-scraper quienbusco --media-metadata --include-location --comments --latest --retry-forever
import json
from pprint import pprint
from fpdf import FPDF

json_data=open("quienbusco-raw-everything/quienbusco.json").read()
data = json.loads(json_data)

# Print front page
pdf = FPDF(orientation = 'P', unit = 'mm', format='A5')
pdf.alias_nb_pages()
pdf.set_font('Helvetica', '', 12)

pdf.add_page()
pdf.set_draw_color(0, 0, 0)
pdf.rect(x=18, y=18, w=113, h=16, style='D')
pdf.set_draw_color(0, 128, 0)
pdf.rect(x=20, y=20, w=113, h=16, style='D')

# Title
pdf.set_font('Helvetica', 'B', 32)
pdf.set_text_color(0, 128, 0)
pdf.text(44, 30, 'Quien Busco')

# Front page image
pdf.image('images/clarence-no-lines.png', x=30, y=55, w=88, h=88, type='png')

# Author
pdf.set_font('Helvetica', 'B', 8)
pdf.set_text_color(108, 108, 108)

pdf.ln(h = 140)

pdf.cell(15)
pdf.cell(100, 12, 'Haikus', 0, 1, 'C')
pdf.cell(15)
pdf.cell(100, 12, 'by', 0, 1, 'C')
pdf.cell(15)
pdf.cell(100, 12, 'Vyas Soekhoe', 0, 1, 'C')

pdf.output('pdfs/quienbusco-frontpage.pdf', 'F')